package customitems;

import helperclasses.TTTRunde;
import helperclasses.TTTSpieler;
import helperclasses.TeamKind;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;

/**
 * Created by Bloodrayne on 20.04.2017.
 */
public abstract class ICustomItem {

    private ItemStack item = null;
    private String id = "CI";
    private String display_name = "CustomItem";
    private ItemKind type = ItemKind.Undefined_item;
    private ArrayList<TeamKind> forTeams = new ArrayList<>();


    public boolean isForTeam(TeamKind x){
        return forTeams.stream()
                .filter(i -> i == x)
                .count() > 0;
    }

    public ItemStack getItem() {
        return item;
    }

    public void setItem(ItemStack item) {
        this.item = item;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDisplay_name() {
        return display_name;
    }

    public void setDisplay_name(String display_name) {
        this.display_name = display_name;
    }

    public ItemKind getType() {
        return type;
    }

    public void setType(ItemKind type) {
        this.type = type;
    }

    public void addTeam(TeamKind x){
        forTeams.add(x);
    }


    /**
     * Wird ausgelöst sobald ein Spieler das Item bekommt
     * @param x Spieler
     */
    public abstract void onPlayerGiven(TTTSpieler x);

    /**
     * Wird ausgelöst sobald ein Spieler das Item benutzt (Links-/Rechts-Klick)
     * @param x Spieler
     */
    public abstract void onPlayerUse(TTTSpieler x);

    /**
     * Wird ausgelöst, sobald ein Spieler das Item in einem Inventar benutzt
     * @param x Spieler
     */
    public abstract void onInventoryUse(TTTSpieler x);


    /**
     * Wird ausgelöst, sobald ein Spieler das Item irgendwie benutzt hat
     * @param x Spieler
     */
    public abstract void onUsed(TTTSpieler x);

    /**
     * wird ausgelöst sobald ein Item generiert wird
     * @param r
     */
    public abstract void onInit(TTTRunde r);
}
