package customitems;

import net.minecraft.server.v1_10_R1.IInventory;

/**
 * Created by Bloodrayne on 20.04.2017.
 */
public abstract class IInventoryItem extends ICustomItem {

    public IInventoryItem(){
        super();
        this.setType(ItemKind.Inventory_item);
    }

}
