package customitems;

import helperclasses.TTTRunde;

/**
 * Created by Bloodrayne on 20.04.2017.
 */
public abstract class IHUDItem extends ICustomItem{

    public IHUDItem(){
        super();
        this.setType(ItemKind.HUD_item);
    }
}
