package customitems;

/**
 * Created by Bloodrayne on 20.04.2017.
 */
public abstract class IHotbarItem extends ICustomItem{

    public IHotbarItem(){
        super();
        setType(ItemKind.Hotbar_item);
    }
}
