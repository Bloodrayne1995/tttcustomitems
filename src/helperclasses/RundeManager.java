package helperclasses;

import customitems.ItemKind;

import java.util.Arrays;

/**
 * Created by Bloodrayne on 20.04.2017.
 */
public class RundeManager {

    private TTTRunde current_runde = null;
    private ItemManager current_items = null;

    public TTTRunde getCurrent_runde() {
        return current_runde;
    }

    public ItemManager getCurrent_items() {
        return current_items;
    }

    public RundeManager(TTTRunde r, ItemManager im){
        current_runde = r;
        current_items =im;
        ladeRunde();
    }



    private void ladeRunde(){
        Arrays.stream(current_items.getAllItems())
                .forEach(i -> i.onInit(current_runde));

        Arrays.stream(current_runde.getTraitors())
                .forEach(s -> {

                    //Hotbar-Items geben
                    Arrays.stream(current_items.getTraitorItems())
                            .filter(i -> i.getType() == ItemKind.Hotbar_item)
                            .forEach(i -> {
                                s.getSpieler().getInventory().addItem(i.getItem());
                                i.onPlayerGiven(s);
                            });

                    //HUD-Items starten
                    //TODO: Überlegen wie das aussehen soll (Version X)

                });
    }





}
