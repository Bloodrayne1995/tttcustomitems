package helperclasses;

import net.caseif.flint.challenger.Challenger;
import org.bukkit.entity.Player;

/**
 * Created by Bloodrayne on 20.04.2017.
 */
public class TTTSpieler {

    /**
     * Bukkit Player-Object
     */
    private Player spieler = null;


    private String debug_name = "DEBUG";
    private boolean debug_mode = false;


    /**
     * Current Team of Player
     */
    private TeamKind team_id = TeamKind.UNDEFINED;


    public Player getSpieler() {
        return spieler;
    }

    public void setSpieler(Player spieler) {
        this.spieler = spieler;
    }

    public TeamKind getTeam_id() {
        return team_id;
    }

    public void setTeam_id(TeamKind team_id) {
        this.team_id = team_id;
    }

    public boolean isTraitor(){
        return getTeam_id() == TeamKind.TRAITOR;
    }

    public boolean isInnocent(){
        return getTeam_id() == TeamKind.INNONCENT;
    }

    public boolean isDetective(){
        return getTeam_id() == TeamKind.DETECTIVE;
    }

    public boolean isUndefined(){
        return getTeam_id() == TeamKind.UNDEFINED;
    }

    public void ladeSpieler(Challenger r, TeamKind teamID){
        team_id = teamID;

    }

    public String getName(){
        if(debug_mode){
            return  debug_name;
        }else{
            return spieler.getName();
        }
    }

    public void ladeDebugSpieler(String name, TeamKind team){
        team_id = team;
        debug_name = name;
        debug_mode = true;
    }

}
