package helperclasses;



import customitems.ICustomItem;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;

/**
 * Created by Bloodrayne on 20.04.2017.
 */
public class ItemManager {

    private ArrayList<ICustomItem> items = new ArrayList<>();

    public ICustomItem getItemById(String id){
        ICustomItem x = null;
        try {
            x = items.stream()
                    .filter(i -> i.getId().equalsIgnoreCase(id))
                    .findFirst().get();
        }catch (NullPointerException ex){
            x = null;
        }
        return x;
    }

    public ICustomItem[] getAllItems(){
        return (ICustomItem[]) items.toArray();
    }

    public ICustomItem[] getTraitorItems(){
        return getItemsOfSpecificTeam(TeamKind.TRAITOR);
    }

    public ICustomItem[] getInnocentItems(){
        return getItemsOfSpecificTeam(TeamKind.INNONCENT);
    }

    public ICustomItem[] getDetectiveItems(){
        return  getItemsOfSpecificTeam(TeamKind.DETECTIVE);
    }

    private ICustomItem[] getItemsOfSpecificTeam(TeamKind a){
        return (ICustomItem[]) items.stream()
                .filter(i -> i.isForTeam(a))
                .toArray();
    }


    public void addItem(ICustomItem i){
        items.add(i);
    }

    public void removeItem(String id){
        items.stream()
                .filter(i -> i.getId().equalsIgnoreCase(id))
                .forEach(items::remove);
    }


    public ICustomItem getItemByStackAndName(ItemStack ism, String dName){
        return items.stream()
                .filter(i -> i.getItem().isSimilar(ism) && i.getDisplay_name().equalsIgnoreCase(dName))
                .findFirst().get();
    }


}
