package helperclasses;


import net.caseif.flint.round.Round;

import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by Bloodrayne on 20.04.2017.
 */
public class TTTRunde {

    private ArrayList<TTTSpieler> spieler = new ArrayList<TTTSpieler>();

    public TTTSpieler[] getTraitors(){
        ArrayList<TTTSpieler> data = new ArrayList<TTTSpieler>();
        spieler.stream()
                .filter(s->s.isTraitor())
                .forEach(data::add);
        return  (TTTSpieler[]) data.toArray();
    }

    public TTTSpieler[] getInnocent(){
        ArrayList<TTTSpieler> data = new ArrayList<TTTSpieler>();
        spieler.stream()
                .filter(s->s.isInnocent())
                .forEach(data::add);
        return  (TTTSpieler[]) data.toArray();
    }

    public TTTSpieler[] getDetectives(){
        ArrayList<TTTSpieler> data = new ArrayList<TTTSpieler>();
        spieler.stream()
                .filter(s->s.isDetective())
                .forEach(data::add);
        return  (TTTSpieler[]) data.toArray();
    }

    public TTTSpieler[] getUndefined(){
        ArrayList<TTTSpieler> data = new ArrayList<TTTSpieler>();
        spieler.stream()
                .filter(s->s.isUndefined())
                .forEach(data::add);
        return  (TTTSpieler[]) data.toArray();
    }


    public void loadFromRound(Round r){
        r.getTeams().stream()
                .forEach(t->{
                    TeamKind team_id = getTeamID(t.getName());
                    t.getChallengers().stream().forEach(ch ->{
                        TTTSpieler s = new TTTSpieler();
                        s.ladeSpieler(ch,team_id);
                        spieler.add(s);
                    });
                });
    }

    public void ladeDebugRunde(){
        TTTSpieler traitor = new TTTSpieler();
        TTTSpieler detective = new TTTSpieler();
        TTTSpieler innocent = new TTTSpieler();

        traitor.ladeDebugSpieler("Traitor",TeamKind.TRAITOR);
        detective.ladeDebugSpieler("Detective",TeamKind.DETECTIVE);
        innocent.ladeDebugSpieler("Innocent",TeamKind.INNONCENT);

        spieler.add(traitor);
        spieler.add(detective);
        spieler.add(innocent);
    }

    private TeamKind getTeamID(String name){
        switch (name.toLowerCase()){
            case "traitor":
                return TeamKind.TRAITOR;
            case "innocent":
                return TeamKind.INNONCENT;
            case "detective":
                return TeamKind.DETECTIVE;
            default:
                return TeamKind.UNDEFINED;
        }
    }


    public TTTSpieler getSpielerByID(UUID id){
        return spieler.stream()
                .filter(i -> i.getSpieler().getUniqueId().equals(id))
                .findFirst().get();
    }


}
