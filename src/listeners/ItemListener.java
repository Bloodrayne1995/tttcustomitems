package listeners;


import customitems.ICustomItem;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import plugin.TTTCustomItem;

/**
 * Created by Bloodrayne on 20.04.2017.
 */
public class ItemListener implements Listener {


    public void register(){
        TTTCustomItem.plugin.getServer().getPluginManager().registerEvents(this,TTTCustomItem.plugin);
    }

    public void unregister(){
        HandlerList.unregisterAll(this);
    }


    /**
     * Reacts on interaction with an item
     * @param event event data
     */
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerUse(PlayerInteractEvent event) {
        Player pl = event.getPlayer();
        ItemStack ist = event.getItem();

        ICustomItem i = TTTCustomItem.plugin.getRundeManager().getCurrent_items().getItemByStackAndName(ist,ist.getItemMeta().getDisplayName());
        if(i != null){
            i.onPlayerUse(TTTCustomItem.plugin.getRundeManager().getCurrent_runde().getSpielerByID(pl.getUniqueId()));
        }

    }

    /**
     * Reacts on an inventory click
     * @param event event data
     */
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onInventoryClick(InventoryClickEvent event) {
        Player pl = (Player) event.getWhoClicked();
        ItemStack ist = event.getCurrentItem();

        ICustomItem i = TTTCustomItem.plugin.getRundeManager().getCurrent_items().getItemByStackAndName(ist,ist.getItemMeta().getDisplayName());
        if(i != null){
            i.onInventoryUse(TTTCustomItem.plugin.getRundeManager().getCurrent_runde().getSpielerByID(pl.getUniqueId()));
        }

    }


}
