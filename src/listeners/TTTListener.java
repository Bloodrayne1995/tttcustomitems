package listeners;

import com.google.common.eventbus.Subscribe;
import helperclasses.TTTRunde;
import net.caseif.flint.event.round.RoundChangeLifecycleStageEvent;
import net.caseif.ttt.util.constant.Stage;
import plugin.TTTCustomItem;

/**
 * Created by Bloodrayne on 20.04.2017.
 */
public class TTTListener {


    @Subscribe
    public void onRoundChangeLifecycleStage(RoundChangeLifecycleStageEvent event) {
        if (event.getStageAfter() == Stage.PLAYING) {
            //TODO: Was soll gemacht werden wenn eine Runde anfängt
            TTTRunde runde = new TTTRunde();
            runde.loadFromRound(event.getRound());

            TTTCustomItem.plugin.onRundeStarted(runde);
        }else{

        }
    }

}
