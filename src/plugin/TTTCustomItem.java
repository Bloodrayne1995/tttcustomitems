package plugin;

import helperclasses.ItemManager;
import helperclasses.RundeManager;
import helperclasses.TTTRunde;
import listeners.ItemListener;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by Bloodrayne on 20.04.2017.
 */
public class TTTCustomItem extends JavaPlugin {

    public static TTTCustomItem plugin = null;

    private ItemManager itemManager = new ItemManager();
    private ItemListener itemListener = new ItemListener();

    public RundeManager getRundeManager() {
        return rundeManager;
    }

    private RundeManager rundeManager = null;

    public ItemManager getItemManager() {
        return itemManager;
    }

    @Override
    public void onDisable() {
        super.onDisable();
        itemListener.unregister();
    }

    @Override
    public void onEnable() {
        super.onEnable();
        TTTCustomItem.plugin = this;

        itemListener.register();

    }


    public void onRundeStarted(TTTRunde r){
        rundeManager = new RundeManager(r,itemManager);

    }
}
